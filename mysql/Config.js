var mysql = require('mysql');
// const MysqlPoolBooster = require('mysql-pool-booster');
// mysql = MysqlPoolBooster(mysql);
var pool = require('mysql-pool-cluster');

var poolCluster = mysql.createPoolCluster({
    removeNodeErrorCount: 1, // Remove the node immediately when connection fails.
    defaultSelector: 'PP3S',
    nodes : [{
        clusterId : 'PP3S',
        host     : 'medppss.medicine.psu.ac.th',
        user     : 'medppss',
        password : 'med1122db',
        database : 'medppss',
        port: 3306
    }, {
        clusterId : 'Developer_db',
        host     : 'medppss.medicine.psu.ac.th',
        user     : 'medppss',
        password : 'med1122db',
        database : 'public',
        port: 3306
    }]
});

/*var poolCluster = mysql.createConnection({
    removeNodeErrorCount: 1, // Remove the node immediately when connection fails.
    defaultSelector: 'PP3S',
})*/
// poolCluster.add(config); // add configuration with automatic name
/*poolCluster.add('PP3S', {
    host     : 'medppss.medicine.psu.ac.th',
    user     : 'medppss',
    password : 'med1122db',
    database : 'medppss',
    port: '3306'
}); // add a named configuration
poolCluster.add('Developer_db', {
    host     : 'medppss.medicine.psu.ac.th',
    user     : 'medppss',
    password : 'med1122db',
    database : 'public',
    port: '3306'
});*/

/*var pool_pp3s  = mysql.createPool({
    host     : 'medppss.medicine.psu.ac.th',
    user     : 'medppss',
    password : 'med1122db',
    database : 'medppss'
});

var pool_pp3s_b  = mysql.createPool({
    host     : '172.29.30.59',
    user     : 'public',
    password : 'public',
    database : 'public'
});*/

exports.pool = {
    poolCluster: poolCluster
};