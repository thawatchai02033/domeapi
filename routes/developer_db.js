var express = require('express');
var router = express.Router();
var pool = require('../mysql/Config').pool.pool_pp3s_b;
var bodyParser = require('body-parser');
var mysql = require('mysql');

router.use(bodyParser.json())

function GetConfigConnection (db) {
    return mysql.createPool({
        host     : '172.29.30.59',
        user     : 'public',
        password : 'public',
        database : db
    })
}

/* GET home page. */
router.put('/:db/:table', function(req, res, next) {

    var field = ''
    var values = ''

    pool = GetConfigConnection(req.params.db)

    if(req.body.data){
        Object.keys(req.body.data).forEach(function(key,idx) {
            if(idx === Object.keys(req.body.data).length - 1){
                var obj = req.body.data[key];
                field += key
                values += "'" + req.body.data[key] + "'"
            } else {
                var obj = req.body.data[key];
                field += key + ", "
                values += "'" + req.body.data[key] + "', "
            }
        });
    }



    pool.getConnection(function(err, connection) {
        if (err) throw err;

        // Use the connection
        connection.query('insert into ' + req.params.table + ' (' + field + ') value (' + values + ')', function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error){
                res.json({
                    status: false
                })
            } else {
                res.json({
                    status: true
                })
            }
            // Don't use the connection here, it has been returned to the pool.
        });
    });
    // console.log(req.params.db)
});

router.post('/:db/:table/:condi1/:value1', function(req, res, next) {

    var condi = ' set '
    pool = GetConfigConnection(req.params.db)

    if(req.body.data){
        Object.keys(req.body.data).forEach(function(key,idx) {
            if(idx === Object.keys(req.body.data).length - 1){
                var obj = req.body.data[key];
                condi += key + ' = "' + req.body.data[key] + '"'
            } else {
                var obj = req.body.data[key];
                condi += key + ' = "' + req.body.data[key] + '" , '
            }
        });
    }

    pool.getConnection(function(err, connection) {
        if (err) throw err;

        // Use the connection
        connection.query('update ' + req.params.table + condi + ' where ' + req.params.condi1 + ' = ' + req.params.value1, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
                res.json({
                    status: false
                })
            } else {
                res.json({
                    status: true
                })
            }

            // Don't use the connection here, it has been returned to the pool.
        });
    });
    // console.log(req.params.db)
});

router.delete('/:db/:table/:condi1/:value1', function(req, res, next) {

    pool = GetConfigConnection(req.params.db)

    pool.getConnection(function(err, connection) {
        if (err) throw err;

        // Use the connection
        connection.query('delete from ' + req.params.table + ' where ' + req.params.condi1 + ' = ' + req.params.value1, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error){
                res.json({
                    status: false
                })
            } else {
                res.json({
                    status: true
                })
            }
            // Don't use the connection here, it has been returned to the pool.
        });
    });
    // console.log(req.params.db)
});

router.get('/:db/:table', function(req, res, next) {
    var condi = ''
    pool = GetConfigConnection(req.params.db)

    if(req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if(req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    pool.getConnection(function(err, connection) {

        // Use the connection
        connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error){
                res.json([{}])
            } else {
                res.json(results)
            }
            // Don't use the connection here, it has been returned to the pool.
        });
    });
    // console.log(req.params.db)
});

router.get('/:db/:table/:condi1/:value1', function(req, res, next) {

    var condi = ' WHERE ' + req.params.condi1 + ' = ' + req.params.value1
    pool = GetConfigConnection(req.params.db)

    if(req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if(req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    pool.getConnection(function(err, connection) {

        if (err) throw err;

        // Use the connection
        connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error){
                res.json([{}])
            } else {
                res.json(results)
            }
            // Don't use the connection here, it has been returned to the pool.
        });
    });
});

router.get('/:db/:table/:condi1/:value1/:condi2/:value2', function(req, res, next) {

    var condi = ' WHERE ' + req.params.condi1 + ' = ' + req.params.value1 + ' AND ' + req.params.condi2 + ' = ' + req.params.value2
    pool = GetConfigConnection(req.params.db)

    if(req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if(req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    pool.getConnection(function(err, connection) {

        if (err) throw err;

        // Use the connection
        connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error){
                res.json([{}])
            } else {
                res.json(results)
            }
            // Don't use the connection here, it has been returned to the pool.
        });
    });
});

router.get('/:db/:table/:condi1/:value1/:condi2/:value2/:condi3/:value3', function(req, res, next) {

    var condi = ' WHERE ' + req.params.condi1 + ' = ' + req.params.value1 + ' AND ' + req.params.condi2 + ' = ' + req.params.value2 + ' AND ' + req.params.condi3 + ' = ' + req.params.value3
    pool = GetConfigConnection(req.params.db)

    if(req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if(req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    pool.getConnection(function(err, connection) {

        if (err) throw err;

        // Use the connection
        connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error){
                res.json([{}])
            } else {
                res.json(results)
            }
            // Don't use the connection here, it has been returned to the pool.
        });
    });
});


module.exports = router;
