var express = require('express');
var router = express.Router();
// var poolCluster = require('../mysql/Config').pool.poolCluster;
var bodyParser = require('body-parser');
var mysql = require('mysql');
var poolCluster = mysql.createPoolCluster();
var multer = require('multer');
var path = require('path');
const formidable = require('formidable');

var upload2 = multer({dest: 'uploads/'})

var ArrayFileName = []

router.use(bodyParser.json({limit: '500mb'}));
router.use(bodyParser.urlencoded({limit: '500mb', extended: true}));

poolCluster.add('PP3S', {
    host: 'medppss.medicine.psu.ac.th',
    user: 'medppss',
    password: 'med1122db',
    database: 'carpark',
    port: '3306'
}); // add a named configuration
poolCluster.add('Developer_db', {
    host: '172.29.30.59',
    user: 'public',
    password: 'public',
    database: 'public',
    port: '3306'
});

router.use(bodyParser.json())

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
        // cb(null, Date.now().toLocaleString() + file.originalname);
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        ArrayFileName.unshift({
            name: file.fieldname + '-' + Date.now() + path.extname(file.originalname),
            path: 'uploads/' + file.fieldname + '-' + Date.now() + path.extname(file.originalname)
        })
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg', file.mimetype === 'image/png'){
        cb(null, true)
    } else {
        cb(null, false)
    }
}

const limit = {
    fileSize: 1024 * 1024 * 5
}

var upload = multer({storage: storage})
var config = {}

function GetConfigConnection(connectname, db) {
    if (connectname == 'api_pp3s') {
        return {
            // connectionLimit : 100, //important
            host: 'medppss.medicine.psu.ac.th',
            user: 'medppss',
            password: 'med1122db',
            database: db
        }
    } else if (connectname == 'api_public') {
        return {
            // connectionLimit : 100, //important
            host: '172.29.30.59',
            user: 'public',
            password: 'public',
            database: db
        }
    }
}

router.get('/', (req, res) => {
    res.send(`
    <h2>With <code>"express"</code> npm package</h2>
    <form action="/uploadfile/upload" enctype="multipart/form-data" method="post">
      <div>Text field title: <input type="text" name="title" /></div>
      <div>File: <input type="file" name="someExpressFiles" multiple="multiple" /></div>
      <input type="submit" value="Upload" />
    </form>
  `);
});

router.post('/upload', (req, res, next) => {
    const form = formidable({ multiples: true,uploadDir: 'uploads',filename: (name, ext, part, form) => {
            return Date.now().toString(36) + Math.random().toString(36).substr(2) + '.' + part.mimetype.split('/')[1]
        }});

    form.parse(req, (err, fields, files) => {
        if (err) {
            next(err);
            return;
        }
        res.json({ fields, files });
    });
});

router.post('/single', function (req, res, next) {
    ArrayFileName = []
    upload.single('file')(req,res, function (err) {
        if(err) {
            return res.send({
                status: false,
                massage: 'Uploads Fails',
                data: []
            })
        }
        res.send({
            status: true,
            massage: 'Uploads Success',
            data: ArrayFileName
        })
    })
})

router.post('/multiple',function (req, res, next) {
    ArrayFileName = []
    upload.array('file',2)(req,res, function (err) {
        if(err) {
            return res.send({
                status: false,
                massage: 'Uploads Fails',
                data: []
            })
        }
        res.send({
            status: true,
            massage: 'Uploads Success',
            data: ArrayFileName
        })
    })
})

module.exports = router;