var express = require('express');
var router = express.Router();
// var poolCluster = require('../mysql/Config').pool.poolCluster;
var bodyParser = require('body-parser');
var mysql = require('mysql');
var poolCluster = mysql.createPoolCluster();
var multer = require('multer');
var path = require('path');

poolCluster.add('PP3S', {
    host: 'medppss.medicine.psu.ac.th',
    user: 'medppss',
    password: 'med1122db',
    database: 'carpark',
    port: '3306'
}); // add a named configuration
poolCluster.add('Developer_db', {
    host: '172.29.30.59',
    user: 'public',
    password: 'public',
    database: 'public',
    port: '3306'
});

router.use(bodyParser.json())

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

var upload = multer({storage: storage})
var config = {}

function GetConfigConnection(connectname, db) {
    if (connectname == 'api_pp3s') {
        return {
            // connectionLimit : 100, //important
            host: 'medppss.medicine.psu.ac.th',
            user: 'medppss',
            password: 'med1122db',
            database: db
        }
    } else if (connectname == 'api_public') {
        return {
            // connectionLimit : 100, //important
            host: '172.29.30.59',
            user: 'public',
            password: 'public',
            database: db
        }
    }
}

/* GET home page. */
router.put('/:connectname/:db/:table', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var field = ''
    var values = ''

    // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.body.data) {
        Object.keys(req.body.data).forEach(function (key, idx) {
            if (idx === Object.keys(req.body.data).length - 1) {
                var obj = req.body.data[key];
                field += key
                if (req.body.data[key] == 'CURRENT_TIMESTAMP') {
                    values += req.body.data[key]
                } else if (typeof req.body.data[key] == 'string') {
                    if (req.body.data[key].toLowerCase().indexOf('()') > 0) {
                        values += req.body.data[key]
                    } else {
                        values += "'" + req.body.data[key] + "'"
                    }
                } else if (typeof req.body.data[key] == 'undefined') {
                    values += req.body.data[key]
                } else if (req.body.data[key] == null) {
                    values += req.body.data[key]
                } else {
                    values += "'" + req.body.data[key] + "'"
                }
            } else {
                var obj = req.body.data[key];
                field += key + ", "
                if (req.body.data[key] == 'CURRENT_TIMESTAMP') {
                    values += req.body.data[key] + ", "
                } else if (typeof req.body.data[key] == 'string') {
                    if (req.body.data[key].toLowerCase().indexOf('()') > 0) {
                        values += req.body.data[key] + ", "
                    } else {
                        values += "'" + req.body.data[key] + "', "
                    }
                } else if (typeof req.body.data[key] == 'undefined') {
                    values += req.body.data[key] + ", "
                } else if (req.body.data[key] == null) {
                    values += req.body.data[key] + ", "
                } else {
                    values += "'" + req.body.data[key] + "', "
                }

            }
        });
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: [{message: 'Connection Name Not Found'}]})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('insert into ' + req.params.table + ' (' + field + ') value (' + values + ')', function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: {message: 'Insert Data Success',last_id: results.insertId}})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
    // console.log(req.params.db)
});

router.post('/:connectname/:db/:table/:condi1/:value1', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var condi = ' set '
    // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.body.data) {
        Object.keys(req.body.data).forEach(function (key, idx) {
            if (idx === Object.keys(req.body.data).length - 1) {
                var obj = req.body.data[key];

                if (req.body.data[key] == 'CURRENT_TIMESTAMP') {
                    condi += key + ' = ' + req.body.data[key]
                } else if (typeof req.body.data[key] == 'string') {
                    if (req.body.data[key].toLowerCase().indexOf('()') > 0) {
                        condi += key + ' = ' + req.body.data[key]
                    } else {
                        condi += key + ' = "' + req.body.data[key] + '"'
                    }
                } else if (typeof req.body.data[key] == 'undefined') {
                    condi += key + ' = ' + req.body.data[key]
                } else if (req.body.data[key] == null) {
                    condi += key + ' = ' + req.body.data[key]
                } else {
                    condi += key + ' = "' + req.body.data[key] + '"'
                }
            } else {
                var obj = req.body.data[key];

                if (req.body.data[key] == 'CURRENT_TIMESTAMP') {
                    condi += key + ' = ' + req.body.data[key] + '" , '
                } else if (typeof req.body.data[key] == 'string') {
                    if (req.body.data[key].toLowerCase().indexOf('()') > 0) {
                        condi += key + ' = ' + req.body.data[key] + '" , '
                    } else {
                        condi += key + ' = "' + req.body.data[key] + '" , '
                    }
                } else if (typeof req.body.data[key] == 'undefined') {
                    condi += key + ' = ' + req.body.data[key] + '" , '
                } else if (req.body.data[key] == null) {
                    condi += key + ' = ' + req.body.data[key] + '" , '
                } else {
                    condi += key + ' = "' + req.body.data[key] + '" , '
                }
            }
        });
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: [{message: 'Connection Name Not Found'}]})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('update ' + req.params.table + condi + ' where ' + req.params.condi1 + ' = ' + req.params.value1, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: {message: 'Update Data Success'}})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
    // console.log(req.params.db)
});

router.post('/:connectname/:db/:table/:condi1/:value1/:condi2/:value2', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var condi = ' set '
    // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.body.data) {
        Object.keys(req.body.data).forEach(function (key, idx) {
            if (idx === Object.keys(req.body.data).length - 1) {
                var obj = req.body.data[key];

                if (req.body.data[key] == 'CURRENT_TIMESTAMP') {
                    condi += key + ' = ' + req.body.data[key]
                } else if (typeof req.body.data[key] == 'string') {
                    if (req.body.data[key].toLowerCase().indexOf('()') > 0) {
                        condi += key + ' = ' + req.body.data[key]
                    } else {
                        condi += key + ' = "' + req.body.data[key] + '"'
                    }
                } else if (typeof req.body.data[key] == 'undefined') {
                    condi += key + ' = ' + req.body.data[key]
                } else if (req.body.data[key] == null) {
                    condi += key + ' = ' + req.body.data[key]
                } else {
                    condi += key + ' = "' + req.body.data[key] + '"'
                }
            } else {
                var obj = req.body.data[key];

                if (req.body.data[key] == 'CURRENT_TIMESTAMP') {
                    condi += key + ' = ' + req.body.data[key] + '" , '
                } else if (typeof req.body.data[key] == 'string') {
                    if (req.body.data[key].toLowerCase().indexOf('()') > 0) {
                        condi += key + ' = ' + req.body.data[key] + '" , '
                    } else {
                        condi += key + ' = "' + req.body.data[key] + '" , '
                    }
                } else if (typeof req.body.data[key] == 'undefined') {
                    condi += key + ' = ' + req.body.data[key] + '" , '
                } else if (req.body.data[key] == null) {
                    condi += key + ' = ' + req.body.data[key] + '" , '
                } else {
                    condi += key + ' = "' + req.body.data[key] + '" , '
                }
            }
        });
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: [{message: 'Connection Name Not Found'}]})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {

                            connection.query('update ' + req.params.table + condi + ' where ' + req.params.condi1 + ' = ' + req.params.value1 + ' AND ' + req.params.condi2 + ' = ' + req.params.value2, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: {message: 'Update Data Success'}})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
    // console.log(req.params.db)
});

router.delete('/:connectname/:db/:table/:condi1/:value1', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    // config = GetConfigConnection(req.params.connectname, req.params.db)
    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: [{message: 'Connection Name Not Found'}]})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('delete from ' + req.params.table + ' where ' + req.params.condi1 + ' = ' + req.params.value1, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: {message: 'Delete Data Success'}})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
    // console.log(req.params.db)
});

router.delete('/:connectname/:db/:table/:condi1/:value1/:condi2/:value2', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    // config = GetConfigConnection(req.params.connectname, req.params.db)
    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: [{message: 'Connection Name Not Found'}]})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('delete from ' + req.params.table + ' where ' + req.params.condi1 + ' = ' + req.params.value1 + ' AND ' + req.params.condi2 + ' = ' + req.params.value2, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: {message: 'Delete Data Success'}})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
    // console.log(req.params.db)
});

router.get('/:connectname/:db/:table', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var condi = ''
    // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.query.where != null) {
        condi += ' WHERE ' + req.query.where.substring(req.query.where.indexOf('{') + 1, req.query.where.lastIndexOf('}'))
    }

    if (req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if (req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: {message: 'Connection Name Not Found'}})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: results})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
});

router.get('/:connectname/:db/:table/:condi1/:value1', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var condi = ' WHERE ' + req.params.condi1 + ' = ' + req.params.value1
    // // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.query.where != null) {
        condi += ' AND ' + req.query.where.substring(req.query.where.indexOf('{') + 1, req.query.where.lastIndexOf('}'))
    }

    if (req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if (req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: [{message: 'Connection Name Not Found'}]})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: results})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
});

router.get('/:connectname/:db/:table/:condi1/:value1/:condi2/:value2', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var condi = ' WHERE ' + req.params.condi1 + ' = ' + req.params.value1 + ' AND ' + req.params.condi2 + ' = ' + req.params.value2
    // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.query.where != null) {
        condi += ' AND ' + req.query.where.substring(req.query.where.indexOf('{') + 1, req.query.where.lastIndexOf('}'))
    }

    if (req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if (req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: {message: 'Connection Name Not Found'}})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: results})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
});

router.get('/:connectname/:db/:table/:condi1/:value1/:condi2/:value2/:condi3/:value3', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var condi = ' WHERE ' + req.params.condi1 + ' = ' + req.params.value1 + ' AND ' + req.params.condi2 + ' = ' + req.params.value2 + ' AND ' + req.params.condi3 + ' = ' + req.params.value3
    // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.query.where != null) {
        condi += ' AND ' + req.query.where.substring(req.query.where.indexOf('{') + 1, req.query.where.lastIndexOf('}'))
    }

    if (req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if (req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: {message: 'Connection Name Not Found'}})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: results})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
});

router.get('/:connectname/:db/:table/:condi1/:value1/:condi2/:value2/:condi3/:value3/:condi4/:value4', function (req, res, next) {
    console.log('here')
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var condi = ' WHERE ' + req.params.condi1 + ' = ' + req.params.value1 + ' AND ' + req.params.condi2 + ' = ' + req.params.value2 + ' AND ' + req.params.condi3 + ' = ' + req.params.value3 + ' AND ' + req.params.condi4 + ' = ' + req.params.value4
    // config = GetConfigConnection(req.params.connectname, req.params.db)

    if (req.query.where != null) {
        condi += ' AND ' + req.query.where.substring(req.query.where.indexOf('{') + 1, req.query.where.lastIndexOf('}'))
    }

    if (req.query.sort != null) {
        condi += ' ORDER BY ' + req.query.sort.split('*')[0] + ' ' + req.query.sort.split('*')[1]
    }

    if (req.query.limit != null) {
        condi += ' limit ' + req.query.limit
    }

    poolCluster.getConnection(req.params.connectname, function (err, connection) {
        if (err) {
            res.json({status: false, data: {message: 'Connection Name Not Found'}})
        } else {
            try {
                connection.changeUser({database: req.params.db}, function (err) {
                    if (err) {
                        res.json({status: false, data: {message: 'Database Name Not Found'}})
                    } else {
                        if (connection) {
                            connection.query('SELECT * FROM ' + req.params.table + condi, function (error, results, fields) {
                                // When done with the connection, release it.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    res.json({
                                        status: false,
                                        data: {message: 'Exception at Query Connection Plesae Check Command'}
                                    })
                                } else {
                                    res.json({status: true, data: results})
                                }
                                // Don't use the connection here, it has been returned to the pool.
                            });

                        }
                    }
                });
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    });
});

router.post('/uploadfile', upload.single('file'), function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    if (!req.file) {
        return res.send('Please select an image to upload');
    }

    res.send(`You have uploaded this image: <hr/><img src="${req.file.path}" width="500"><hr /><a href="./">Upload another image</a>`);

})

router.get('/files/:uid', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    res.sendFile('/var/www/api/files/' + req.params.uid + '.pdf', (err) => {
    // res.sendFile('/Users/janthip/IdeaProjects/domeapi/files/' + req.params.uid + '.pdf', (err) => {
        if (err) {
            res.status(404).send('Not found');
        }
    })
})

/*router.post('/uploadfile', (req, res) => {

    console.log(req.file)

    let upload = multer({ storage: storage }).single('file');

    upload(req, res, function(err) {
        console.log(req.file)
        // req.file contains information of uploaded file
        // req.body contains information of text fields, if there were any

        if (req.fileValidationError) {
            return res.send(req.fileValidationError);
        }
        else if (!req.file) {
            return res.send('Please select an image to upload');
        }
        else if (err instanceof multer.MulterError) {
            return res.send(err);
        }
        else if (err) {
            return res.send(err);
        }

        // Display uploaded image for user validation
        res.send(`You have uploaded this image: <hr/><img src="${req.file.path}" width="500"><hr /><a href="./">Upload another image</a>`);

    });
});*/


module.exports = router;
