var express = require('express');
var router = express.Router();
// var poolCluster = require('../mysql/Config').pool.poolCluster;
var bodyParser = require('body-parser');
var multer = require('multer');
var path = require('path');
const createReport = require('docx-templates').default;
const fs = require('fs')
const imageToBase64 = require('image-to-base64');
const axios = require('axios').default;


var ArrayFileName = []

router.use(bodyParser.json({limit: '500mb'}));
router.use(bodyParser.urlencoded({limit: '500mb', extended: true}));

router.use(bodyParser.json())

router.get('/', async (req, res) => {
    res.send({
        status: false
    })
})

router.get('/year/:year', async (req, res) => {
    var base64data = null
    var objArrData = []
    var debtorData = []
    const template = fs.readFileSync('/var/www/api/docxtemplate/docxTemplate.docx');
    // const template = fs.readFileSync('/Users/janthip/IdeaProjects/domeapi/docxtemplate/docxTemplate.docx');
    if (req.params.year != null && req.params.year != '' && req.params.year.split('-').length > 0) {
        axios('https://medppss.medicine.psu.ac.th/api/dome/PP3S/carpark/debtor').then(async resDebtor => {
            if (resDebtor.data.status) {
                debtorData = resDebtor.data.data
            }
            axios('https://medppss.medicine.psu.ac.th/api/dome/PP3S/carpark/member_old_view/year/' + req.params.year.split('-')[0] + '/status/1/pay_money/0/is_used/1').then(async resData => {
                if (resData.data.status) {
                    objArrData = resData.data.data.length > 0 ? resData.data.data.filter(item => item.create_time.split('T')[0] == req.params.year) : []
                    if (objArrData.length > 0) {
                        objArrData.map(async (item, idx) => {
                            item.idx = idx + 1
                            item.deptor = debtorData.length > 0 ? debtorData.filter(itemDepT => itemDepT.uid == item.uid).length > 0 ? true : false : false
                            item.date_renew_th = item.date_renew != null ? dateDbtoThai(item.date_renew.split('T')[0]) : ''
                            // await imageToBase64(`/Users/janthip/IdeaProjects/domeapi/uploads/` + item.file_new_name) // Path to the image
                            await imageToBase64(item.file_path) // Path to the image
                                .then(
                                    (response) => {
                                        item.imageToBase64 = {width: 10, height: 9, data: response, extension: '.png'}
                                    }
                                )
                                .catch(
                                    (error) => {
                                        // console.log(error); // Logs an error if there was one
                                        item.imageToBase6 = null
                                    }
                                )
                            if (idx == objArrData.length - 1) {
                                const buffer = await createReport({
                                    template,
                                    data: {
                                        // name: 'John',
                                        // surname: 'Appleseed',
                                        // image: 'https://medppss.medicine.psu.ac.th/api/uploads/l3ffyi3kdkkyl2gclqq.jpeg',
                                        objArrData: objArrData
                                    },
                                    additionalJsContext: {
                                        qr: async () => {
                                            // base64data = Buffer.from(`/Users/janthip/IdeaProjects/domeapi/uploads/l3fed7p8y39tmz3ns6.png`).toString('base64');
                                            return {width: 5, height: 5, data: base64data, extension: '.png'};
                                        }
                                    }
                                });

                                fs.writeFileSync('/var/www/api/report.docx', buffer)
                                // fs.writeFileSync('/Users/janthip/IdeaProjects/domeapi/report.docx', buffer)
                                // res.send({
                                //     status: true
                                // })
                                res.sendFile('/var/www/api/report.docx')
                                // res.sendFile('/Users/janthip/IdeaProjects/domeapi/report.docx')
                            }
                        })
                    } else {
                        res.send({
                            status: false
                        })
                    }

                    // imageToBase64(`/Users/janthip/IdeaProjects/domeapi/uploads/l3fed7p8y39tmz3ns6.png`) // Path to the image
                    //     .then(
                    //         (response) => {
                    //             base64data = response
                    //         }
                    //     )
                    //     .catch(
                    //         (error) => {
                    //             console.log(error); // Logs an error if there was one
                    //         }
                    //     )
                } else {
                    res.send({
                        status: false
                    })
                }
            }).catch(resCatch => {
                if (resCatch) {
                    res.send({
                        status: false
                    })
                }
            })
        })
    } else {
        res.send({
            status: false
        })
    }
});

router.get('/all/:year', async (req, res) => {
    var base64data = null
    var objArrData = []
    var debtorData = []
    const template = fs.readFileSync('/var/www/api/docxtemplate/docxTemplate.docx');
    // const template = fs.readFileSync('/Users/janthip/IdeaProjects/domeapi/docxtemplate/docxTemplate.docx');
    if (req.params.year != null && req.params.year != '') {
        axios('https://medppss.medicine.psu.ac.th/api/dome/PP3S/carpark/debtor').then(async resDebtor => {
            if (resDebtor.data.status) {
                debtorData = resDebtor.data.data
            }
            axios('https://medppss.medicine.psu.ac.th/api/dome/PP3S/carpark/member_old_view/year/' + req.params.year + '/status/1/pay_money/0/is_used/1').then(async resData => {
                if (resData.data.status) {
                    objArrData = resData.data.data
                    if (objArrData.length > 0) {
                        objArrData.map(async (item, idx) => {
                            item.idx = idx + 1
                            item.deptor = debtorData.length > 0 ? debtorData.filter(itemDepT => itemDepT.uid == item.uid).length > 0 ? true : false : false
                            item.date_renew_th = item.date_renew != null ? dateDbtoThai(item.date_renew.split('T')[0]) : ''
                            // await imageToBase64(`/Users/janthip/IdeaProjects/domeapi/uploads/` + item.file_new_name) // Path to the image
                            await imageToBase64(item.file_path) // Path to the image
                                .then(
                                    (response) => {
                                        item.imageToBase64 = {width: 10, height: 9, data: response, extension: '.png'}
                                    }
                                )
                                .catch(
                                    (error) => {
                                        // console.log(error); // Logs an error if there was one
                                        item.imageToBase6 = null
                                    }
                                )
                            if (idx == objArrData.length - 1) {
                                const buffer = await createReport({
                                    template,
                                    data: {
                                        // name: 'John',
                                        // surname: 'Appleseed',
                                        // image: 'https://medppss.medicine.psu.ac.th/api/uploads/l3ffyi3kdkkyl2gclqq.jpeg',
                                        objArrData: objArrData
                                    },
                                    additionalJsContext: {
                                        qr: async () => {
                                            // base64data = Buffer.from(`/Users/janthip/IdeaProjects/domeapi/uploads/l3fed7p8y39tmz3ns6.png`).toString('base64');
                                            return {width: 5, height: 5, data: base64data, extension: '.png'};
                                        }
                                    }
                                });

                                fs.writeFileSync('/var/www/api/report.docx', buffer)
                                // fs.writeFileSync('/Users/janthip/IdeaProjects/domeapi/report.docx', buffer)
                                // res.send({
                                //     status: true
                                // })
                                res.sendFile('/var/www/api/report.docx')
                                // res.sendFile('/Users/janthip/IdeaProjects/domeapi/report.docx')
                            }
                        })
                    } else {
                        res.send({
                            status: false
                        })
                    }

                    // imageToBase64(`/Users/janthip/IdeaProjects/domeapi/uploads/l3fed7p8y39tmz3ns6.png`) // Path to the image
                    //     .then(
                    //         (response) => {
                    //             base64data = response
                    //         }
                    //     )
                    //     .catch(
                    //         (error) => {
                    //             console.log(error); // Logs an error if there was one
                    //         }
                    //     )
                } else {
                    res.send({
                        status: false
                    })
                }
            }).catch(resCatch => {
                if (resCatch) {
                    res.send({
                        status: false
                    })
                }
            })
        })
    } else {
        res.send({
            status: false
        })
    }
});

router.get('*', function(req, res){
    res.send({
        status: false
    }, 404)
});

function dateDbtoThai(value) {
    // const dateDb = value.split(' ')[0]
    var year = value.split('-')[0]
    var month = value.split('-')[1]
    var date = value.split('-')[2]

    const monthArr = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']
    return date + ' ' + monthArr[parseInt(month) - 1] + ' ' + (parseInt(year) + 543)
}

module.exports = router;