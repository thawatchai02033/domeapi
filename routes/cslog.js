var express = require('express');
var router = express.Router();
var querystring = require('querystring');
var axios = require('axios');
// var Cookies = '';

let cookieParser = require('cookie-parser');
//setup express app
let app = express()

app.use(cookieParser());

router.get('/', function (req, res, next) {
    // res.json({status: true, data: 'Hello This is GetData Form CS API'})
    // res.cookie("userData", users);
    // res.send(req.cookies);
    // res.clearCookie('userData');
    // res.clearCookie('Cookie');
    res.send(req.cookies);
})

router.get('/:time/:door_name', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    const param = {
        "status": '1',
        "packageName": "acc",
        "systemCode": "acc",
        "pager.posStart": "0",
        "pager.pageSize": "9999",
        "accTransaction.eventTime": req.params.time + " 00:00:00",
        "filter:eventTime": req.params.time + " 23:59:59",
        "accTransaction.eventPointName": req.params.door_name,
    }

    getCookies().then(resCookie => {
        if (resCookie.data.status) {
            res.cookie("Cookie", resCookie.data.data);
            getDataFormCS(param, resCookie.data.data).then(resData => {
                if(resData.data.status) {
                    res.clearCookie('Cookie');
                    res.json({
                        status: true,
                        data: resData.data
                    })
                }
            })
        } else {
            res.clearCookie('Cookie');
            res.json({
                status: false,
                data: {message: 'Exception Get Cookie not work'}
            })
        }
    })
})

function getDataFormCS(param, Cookies) {
    return new Promise(function (resolutionFunc, rejectionFunc) {
        var header = {
            headers: {
                Cookie: Cookies,
                withCredentials: true
            }
        }

        try {
            axios.post('http://172.29.154.252:8088/accTransactionAction!getAll.action', querystring.stringify(param), header).then(function(res) {
                if(res.status == 201)
                {
                    resolutionFunc({data: {status:false,data: null}})
                }
                else
                {
                    resolutionFunc({data: {status:true,data:res.data}})
                }
            })
        } catch (e) {
            console.log(e)
        } finally {

        }
    })
}

function getCookies() {
    return new Promise(function (resolutionFunc, rejectionFunc) {
        axios.post('http://172.29.154.252:8088/authLoginAction!login.do?username=chaiyan&password=chaiyan2020', '').then(function(res) {
            if(res.data.ret == 'ok') {
                resolutionFunc({data: {status:true,data:res.headers['set-cookie'][0] + ':' + res.headers['set-cookie'][1] + ':' + res.headers['set-cookie'][2]}})
            } else {
                rejectionFunc({data: {status:false,data: null}})
            }
        })
    })
}

module.exports = router;
